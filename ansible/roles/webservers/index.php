<?php
$config = parse_ini_file("config.ini");

$servername = $config[db_server];
$username = $config[db_username];
$password = $config[db_password];

echo "Connecting to $servername<br>";

$conn = new mysqli($servername, $username, $password);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
echo "Connected successfully";

?>